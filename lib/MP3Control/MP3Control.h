#ifndef MP3Control_h
#define MP3Control_h

#include <Arduino.h>

class MP3Control
{
    public:
        MP3Control();
        void bind(int pins[], int numPins = 5);
        void start(int index);
        void stop();
        void update(unsigned long deltaTime);
    private:
        //long _interval;
        //long _remainedTime;
        bool _isRunning;
        int *_pins;
        int _numPins;
};

#endif