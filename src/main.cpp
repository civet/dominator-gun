#include <Arduino.h>
#include "PushButton.h"
#include "MP3Control.h"
#include "Marquee.h"
#include "Fader.h"

// marco for compute the number of elements in a statically-allocated array.
#define COUNTOF(array) (sizeof(array) / sizeof(array[0]))

// t0: previousTime, t1: currentTime
unsigned long t0, t1, deltaTime;


PushButton button;
MP3Control player;
Marquee marquee;
Fader fader;
unsigned long pressedAt;

int buttonPin = 2; // button pin only
int faderPin = 3; // PWM pin for fade-in
int marqueePins[] = {5, 6, 9, 10, 11}; // PWM pins for marquee
int playerPins[] = {A1, A2, A3, A4, A5}; // connected to mp3 module pins(A1,A2,...A5)

void setup()
{
    Serial.begin(9600);

    // wait a moment
    delay(2000);
    
    // init
    button.bind(buttonPin);
    player.bind(playerPins);
    marquee.bind(marqueePins, COUNTOF(marqueePins));
    fader.bind(faderPin, true);

    // light on
    marquee.start(); // default - interval:30, delta:5
    fader.start(250, 1); // default - interval:30, delta:5

    // play sound
    player.start(1);

    // handle button event
    button.onPress([]() {
        pressedAt = millis();
    });
    button.onRelease([]() {
        unsigned long t = millis() - pressedAt;
        int i;

        //Serial.print("pressedTime: ");
        //Serial.println(t);

        if(t < 1000) {
            i = (int)(random(2, 4)); // range: [min, max)
            player.start(i);
        }
        else if(t >= 1000 && t < 3000) {
            i = (int)(random(4, 6)); // range: [min, max)
            player.start(i); 
        }
        else if(t >= 3000) {
            player.stop();
        }
        
    });

    // start looping
    t0 = millis();
}

void loop()
{
    t1 = millis();
    deltaTime = t1 - t0;
    t0 = t1;

    button.update(deltaTime);
    player.update(deltaTime);
    marquee.update(deltaTime);
    fader.update(deltaTime);
}
