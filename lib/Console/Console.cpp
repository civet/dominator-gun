#include <Arduino.h>
#include "Console.h"

Console::Console(unsigned long baud)
{
    _baud = baud;
}

void Console::init()
{
    Serial.begin(_baud);
}

void Console::dispose() 
{
    Serial.end();
}

void Console::log(String message)
{
    Serial.println(message);
}