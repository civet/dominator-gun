#include <Arduino.h>
#include "PushButton.h"

#define DELAY_TIME 50;

PushButton::PushButton()
{
    _remainedTime = DELAY_TIME;
}

void PushButton::bind(int pin) 
{
    _pin = pin;
    _lastState = HIGH; // Keep in mind the default state is HIGH

    // This configuration causes the input to read 
    // HIGH when the switch is open, and LOW when it is closed.
    pinMode(_pin, INPUT_PULLUP);
}

void PushButton::onPress(void (*callback)(void))
{
    _pressHandler = callback;
}

void PushButton::onRelease(void (*callback)(void))
{
    _releaseHandler = callback;  
}

void PushButton::update(unsigned long deltaTime)
{
    // Delay a little bit to avoid bouncing
    //delay(50); 

    _remainedTime -= deltaTime;
    if(_remainedTime > 0) return;
    else _remainedTime = DELAY_TIME;
    
    _state = digitalRead(_pin);
    if(_state != _lastState) {
        if(_state == LOW) {
            if(_pressHandler) _pressHandler();
        }
        else {
            if(_releaseHandler) _releaseHandler();
        }   
    }
    _lastState = _state;
}