#ifndef Ticker_h
#define Ticker_h

#include <Arduino.h>

class Ticker
{
    public:
        Ticker();
        void start(long interval = 1000);
        void stop();
        void reset();
        void onTick(void (*callback)(int));
        void update(unsigned long deltaTime);
    private:
        long _interval;
        long _remainedTime;
        bool _isRunning;
        int _repeatCount;
        void (*_tickHandler)(int);
};

#endif