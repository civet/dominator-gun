#include <Arduino.h>
#include "Fader.h"

Fader::Fader()
{
   _isRunning = false;
}

void Fader::bind(int pin, bool fadeInOnly) 
{
    _pin = pin;
    _value = 0;

    _fadeInOnly = fadeInOnly;

    pinMode(_pin, OUTPUT);
    analogWrite(_pin, _value);
}

void Fader::start(long interval, int delta)
{
    if(_isRunning) return;

    _isRunning = true;    
    _interval = interval;
    _remainedTime = _interval;
    _value = 0;
    _delta = delta;
    _sign = 1;

    analogWrite(_pin, _value);
}

void Fader::stop()
{
    if(!_isRunning) return;

    _isRunning = false;
    _value = 0;

    analogWrite(_pin, _value);
}

void Fader::update(unsigned long deltaTime)
{
    if(!_isRunning) return;

    _remainedTime -= deltaTime;
    if(_remainedTime <= 0) {
        _remainedTime = _interval;

        _value += _delta * _sign;
        if(_value < 0) {
            _value = 0 + _delta;
            _sign = 1;
        } 
        else if(_value > 255) {
            if(_fadeInOnly) {
                _value = 255;
                _isRunning = false;
            }
            else {
                _value = 255 - _delta;
                _sign = -1;
            }
        }
        
        analogWrite(_pin, _value);
    }
}