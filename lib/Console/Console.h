#ifndef Console_h
#define Console_h

#include <Arduino.h>

class Console
{
    public:
        Console(unsigned long baud);
        void init();
        void dispose();
        void log(String message);
    private:
        unsigned long _baud;
};

#endif