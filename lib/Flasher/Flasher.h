#ifndef Flasher_h
#define Flasher_h

#include <Arduino.h>

class Flasher
{
    public:
        Flasher();
        void bind(int pin);
        void start(long interval = 1000);
        void stop();
        void update(unsigned long deltaTime);
    private:
        int _pin;
        long _interval;
        long _remainedTime;
        bool _isRunning;
        int _state; 
};

#endif