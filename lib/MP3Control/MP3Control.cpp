#include <Arduino.h>
#include "MP3Control.h"

MP3Control::MP3Control()
{
   _isRunning = false;
}

void MP3Control::bind(int pins[], int numPins)
{
    _pins = pins;
    _numPins = numPins;

    int i, pin;
    for(i = 0; i < _numPins; i++) {
        pin = _pins[i];
        
        pinMode(pin, OUTPUT);
        digitalWrite(pin, HIGH);
    }
}

void MP3Control::start(int index)
{
    String code, str;
    code = String(index, BIN);
    while((int)(code.length()) < _numPins) code = String("0" + code);
    
    int i, pin;
    int lastIndex = _numPins - 1;
    for(i = 0; i < _numPins; i++) {
        pin = _pins[i];
        str = code.charAt(lastIndex - i);
        
        digitalWrite(pin, str.equals("1") ? LOW : HIGH);
    }

    delay(500);

    for(i = 0; i < _numPins; i++) {
        pin = _pins[i];

        digitalWrite(pin, HIGH);
    }

    _isRunning = true;
}

void MP3Control::stop()
{
    this->start(31);

    _isRunning = false;
}

void MP3Control::update(unsigned long deltaTime)
{
    return;
}