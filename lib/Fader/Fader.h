#ifndef Fader_h
#define Fader_h

#include <Arduino.h>

class Fader
{
    public:
        Fader();
        void bind(int pin, bool fadeInOnly = false);
        void start(long interval = 30, int delta = 5);
        void stop();
        void update(unsigned long deltaTime);
    private:
        int _pin;
        long _interval;
        long _remainedTime;
        bool _isRunning;
        int _value; // brightness: 0 ~ 255
        int _delta; // fadeAmount
        int _sign;  // -1 or 1
        bool _fadeInOnly;
};

#endif