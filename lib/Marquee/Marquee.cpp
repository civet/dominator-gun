// #define PI 3.1415926535897932384626433832795
// #define HALF_PI 1.5707963267948966192313216916398
// #define TWO_PI 6.283185307179586476925286766559
// #define DEG_TO_RAD 0.017453292519943295769236907684886
// #define RAD_TO_DEG 57.295779513082320876798154814105
#include <Arduino.h>
#include "Marquee.h"

Marquee::Marquee()
{
   _isRunning = false;
   _repeatCount = 0;
}

void Marquee::bind(int pins[], int numPins)
{
    _pins = pins;
    _numPins = numPins;

    int pin;
    for(int i = 0; i < _numPins; i++) {
        pin = _pins[i];

        pinMode(pin, OUTPUT);
        analogWrite(pin, 0);
    }
}

void Marquee::start(long interval, int delta)
{
    if(_isRunning) return;

    _isRunning = true;    
    _interval = interval;
    _remainedTime = _interval;
    _offset = 0;
    _delta = delta;
}

void Marquee::stop()
{
    if(!_isRunning) return;

    _isRunning = false;

    int pin;
    for(int i = 0; i < _numPins; i++) {
        pin = _pins[i];

        analogWrite(pin, 0);
    }
}

void Marquee::reset()
{
    this->stop();
    _repeatCount = 0;
}

void Marquee::update(unsigned long deltaTime)
{
    if(!_isRunning) return;

    _remainedTime -= deltaTime;
    if(_remainedTime <= 0) {
        _remainedTime = _interval;

        _repeatCount++;

        // wave
        int pin, numSegments;
        double value;
        for(int i = 0; i < _numPins; i++) {
            pin = _pins[i];
            
            //value = ((255/_numPins) * i + _offset) % 255;
            //use cos() instead
            numSegments = _numPins - 1;
            value = cos(PI / numSegments * i + PI / 180 * _offset) * 0.5 + 0.5;
            value *= 255;
            
            //Serial.print(pin);Serial.print(": ");Serial.print(value);Serial.print("\t");

            analogWrite(pin, (int)value);
        }
        //Serial.println("");

        _offset = (_offset + _delta) % 360;
    }
}