#include <Arduino.h>
#include "Flasher.h"

Flasher::Flasher()
{
   _isRunning = false;
}

void Flasher::bind(int pin) 
{
    _pin = pin;
    _state = LOW;

    pinMode(_pin, OUTPUT);
    digitalWrite(_pin, _state);
}

void Flasher::start(long interval)
{
    if(_isRunning) return;

    _isRunning = true;    
    _interval = interval;
    _remainedTime = _interval;    
    _state = HIGH;

    digitalWrite(_pin, _state);
}

void Flasher::stop()
{
    if(!_isRunning) return;

    _isRunning = false;
    _state = LOW;

    digitalWrite(_pin, _state);    
}

void Flasher::update(unsigned long deltaTime)
{
    if(!_isRunning) return;

    _remainedTime -= deltaTime;
    if(_remainedTime <= 0) {
        _remainedTime = _interval;     
        _state = (_state == LOW) ? HIGH : LOW;
        
        digitalWrite(_pin, _state);
    }
}