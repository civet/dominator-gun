#ifndef PushButton_h
#define PushButton_h

#include <Arduino.h>

class PushButton
{
    public:
        PushButton();
        void bind(int pin);
        void onPress(void (*callback)(void));
        void onRelease(void (*callback)(void));
        void update(unsigned long deltaTime);
    private:
        int _pin;
        long _remainedTime;
        int _state;
        int _lastState;
        void (*_pressHandler)(void);
        void (*_releaseHandler)(void);
};

#endif