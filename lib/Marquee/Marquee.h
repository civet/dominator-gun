#ifndef Marquee_h
#define Marquee_h

#include <Arduino.h>

class Marquee
{
    public:
        Marquee();
        void bind(int pins[], int numPins);
        void start(long interval = 30, int delta = 5);
        void stop();
        void reset();
        void update(unsigned long deltaTime);
    private:
        long _interval;
        long _remainedTime;
        bool _isRunning;
        int _repeatCount;
        int *_pins;
        int _numPins;
        int _offset = 0;
        int _delta = 1;
};

#endif